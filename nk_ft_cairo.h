/* nk_ft_cairo.h - freetype/cairo render for nuklear
 *
 * This file is part of nk_ft_cairo.
 * Copyright 2021 by Richard Gill <richard@houbathecat.fr>
 * SPDX-License-Identifier: GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *
 * nk_ft_cairo is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * nk_ft_cairo is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with nk_ft_cairo.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contains adapted parts from:
 * - nuklear_xcb for cairo integration (MIT License)
 *   Copyright 2017 Adriano Grieb
 *   https://github.com/griebd/nuklear_xcb
 * - twidgets for cairo implementation of NK_COMMAND_RECT_MULTI_COLOR (MIT License)
 *   Copyright (c) 2019-2020 Xichen Zhou
 *   https://github.com/taiwins/twidgets
 */
#ifndef NK_FT_CAIRO_H
#define NK_FT_CAIRO_H

#ifndef CAIRO_H
#include <cairo/cairo.h>
#endif /* !CAIRO_H */

#ifndef NK_NUKLEAR_H_
#include <nuklear.h>
#endif

/* if the user set a font size to 0, this size is used by default */
#ifndef NKFC_DEFAULT_FONT_SIZE
#define NKFC_DEFAULT_FONT_SIZE   12.0
#endif

#define NK_DEG2RAD(x) ((double) x * NK_PI / 180.0)
#define NK_COL2CAIRO(x) ((double) x / 255.0)

#ifdef __cplusplus
extern "C" {
#endif

/* init/term */
NK_API nk_bool nk_ft_cairo_init(nk_size initial_font_capacity);
NK_API void nk_ft_cairo_term();

/* font atlas handling */
NK_API nk_bool nk_ft_cairo_font_load(struct nk_user_font *font, double font_size);
NK_API nk_bool nk_ft_cairo_font_from_file(struct nk_user_font *font, const char *file_name, double font_size);
NK_API void nk_ft_cairo_font_free(struct nk_user_font *font);
NK_API cairo_scaled_font_t *nk_ft_cairo_font_set(cairo_t *cr, const struct nk_user_font *font);

/* rendering on context */
NK_API void nk_ft_cairo_context_enter(cairo_t *cr, struct nk_context *ctx);
NK_API void nk_ft_cairo_context_leave(cairo_t *cr, struct nk_context *ctx);
NK_API void nk_ft_cairo_render(cairo_t *cr, struct nk_context *ctx);

#ifdef __cplusplus
}
#endif

#endif /* !NK_FT_CAIRO_H */

/* ------------------------------------------------------------------------- */
/*                                                                           */
/*                                IMPLEMENTATION                             */
/*                                                                           */
/* ------------------------------------------------------------------------- */
#ifdef NKFC_IMPLEMENTATION

#ifndef CAIRO_FT_H
#include <cairo/cairo-ft.h>
#endif /* !CAIRO_FT_H */

#ifndef NKFC_ASSERT
#ifdef NK_ASSERT
#define NKFC_ASSERT NK_ASSERT
#else
#include <assert.h>
#define NKFC_ASSERT assert
#endif /* !NK_ASSERT */
#endif /* !NKFC_ASSERT */

/* memory management */
#include <stdlib.h>
#include <string.h>

#ifndef NKFC_BUFFER_GROW_FACTOR
#define NKFC_BUFFER_GROW_FACTOR 2.0
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* really trivial auto-grow buffer of elements of fixed size */
typedef struct nkfc_buffer {
    void *mem;
    nk_size item_size;
    nk_size size;
    nk_size allocated;
} nkfc_buffer_t;

NK_INTERN void nkfc_buffer_init(nkfc_buffer_t *b, nk_size item_size, nk_size initial_capacity) {
    NKFC_ASSERT(b);
    b->mem = NULL;
    b->item_size = item_size;
    b->size = 0;
    b->allocated = initial_capacity;
    if (b->allocated > 0) {
        b->mem = calloc(initial_capacity, item_size);
    }
}

NK_INTERN void nkfc_buffer_term(nkfc_buffer_t *b) {
    NKFC_ASSERT(b);
    if (b->mem) {
        free(b->mem);
    }
    memset(b, 0, sizeof(nkfc_buffer_t));
}

NK_INTERN nk_size nkfc_buffer_size(nkfc_buffer_t *b) {
    NKFC_ASSERT(b);
    return b->size;
}

NK_INTERN void *nkfc_buffer_at(nkfc_buffer_t *b, nk_size index) {
    NKFC_ASSERT(b);
    NKFC_ASSERT(index < b->size);
    return b->mem + (index * b->item_size);
}

NK_INTERN nk_size nkfc_buffer_alloc(nkfc_buffer_t *b, void **ptr) {
    nk_size index;
    NKFC_ASSERT(b);
    if (!(b->allocated - b->size)) {
        /* need to reallocate to grow the capacity */
        nk_size capacity = (nk_size)((float)(b->allocated * NKFC_BUFFER_GROW_FACTOR));
        b->mem = realloc(b->mem, capacity * b->item_size);
        b->allocated = capacity;
    }
    index = b->size++;
    *ptr = b->mem + (index * b->item_size);
    return index;
}

/* users can load the same font for different sizes. To make it simple for them,
 * the reference to the loaded file is kept internally and a usage count is
 * incremented for each size.
 */
typedef struct loaded_font {
    const char *file;
    cairo_font_face_t *cairo;
    nk_size usage;
} loaded_font_t;

/* that's our storage of a font used by cairo, and referencing a user provided
 * nuklear font. The actual font is stored in the loaded_font_t struct in case
 * the same font is used for several sizes. We also store the allocated scaled
 * font, which depends on the cairo context, for faster metrics computations.
 */
typedef struct face {
    struct nk_user_font *nkfont;
    loaded_font_t *font;
    cairo_scaled_font_t *scaled_font;
    double font_size;
} face_t;

/* the fonts atlas storing all loaded font */
NK_INTERN struct {
    nkfc_buffer_t fonts;    /* loaded_font_t */
    nkfc_buffer_t faces;    /* face_t */
    nk_bool fixup;
} font_atlas;

/* keep a reference to the opened freetype library to avoid loading it several
 * times.
 */
NK_INTERN FT_Library ft_library = NULL;


NK_INTERN loaded_font_t *font_atlas_font_get(nk_size index) {
    NKFC_ASSERT(index < nkfc_buffer_size(&font_atlas.fonts));
    return (loaded_font_t *)nkfc_buffer_at(&font_atlas.fonts, index);
}

NK_INTERN face_t *font_atlas_face_get(nk_size index) {
    NKFC_ASSERT(index < nkfc_buffer_size(&font_atlas.faces));
    return (face_t *)nkfc_buffer_at(&font_atlas.faces, index);
}

NK_INTERN float font_text_width(nk_handle handle, float height __attribute__ ((__unused__)), const char *text, int len) {
    face_t *face;
    cairo_glyph_t *glyphs = NULL;
    cairo_text_extents_t extents;
    int num_glyphs;

    NKFC_ASSERT(handle.id < nkfc_buffer_size(&font_atlas.faces));
    face = font_atlas_face_get(handle.id);
    NKFC_ASSERT(face->scaled_font);

    cairo_scaled_font_text_to_glyphs(face->scaled_font, 0, 0, text, len, &glyphs, &num_glyphs, NULL, NULL, NULL);
    cairo_scaled_font_glyph_extents(face->scaled_font, glyphs, num_glyphs, &extents);
    cairo_glyph_free(glyphs);

    return extents.x_advance;
}

NK_INTERN nk_size font_atlas_find_font(const char *font_file) {
    nk_size count = nkfc_buffer_size(&font_atlas.fonts);
    for (nk_size i = 0; i < count; ++i) {
        if (!strcmp(font_atlas_font_get(i)->file, font_file)) {
            return i;
        }
    }
    return -1;
}

NK_INTERN void font_atlas_alloc_face(struct nk_user_font *nkfont, loaded_font_t *font, double font_size) {
    face_t *face;
    nk_size index;

    NKFC_ASSERT(nkfont);

    if (font_size < 0.01) {
        font_size = NKFC_DEFAULT_FONT_SIZE;
    }

    index = nkfc_buffer_alloc(&font_atlas.faces, (void **)&face);
    face->nkfont = nkfont;
    face->font = font;
    face->scaled_font = NULL;
    face->font_size = font_size;
    nkfont->userdata.id = index;
    nkfont->height = 0;
    nkfont->width = font_text_width;
    font_atlas.fixup = nk_true;
}


NK_API nk_bool nk_ft_cairo_init(nk_size initial_font_capacity) {
    font_atlas.fixup = 0;
    nkfc_buffer_init(&font_atlas.fonts, sizeof(loaded_font_t), initial_font_capacity);
    nkfc_buffer_init(&font_atlas.faces, sizeof(face_t), initial_font_capacity);
    return nk_true;
}

NK_API void nk_ft_cairo_term() {
    nkfc_buffer_term(&font_atlas.faces);
    nkfc_buffer_term(&font_atlas.fonts);
    if (ft_library != NULL) {
        FT_Done_FreeType(ft_library);
        ft_library = NULL;
    }
}

/* font atlas handling */
NK_API nk_bool nk_ft_cairo_font_load(struct nk_user_font *nkfont, double font_size) {
    font_atlas_alloc_face(nkfont, NULL, font_size);
    return nk_true;
}

NK_API nk_bool nk_ft_cairo_font_from_file(struct nk_user_font *nkfont, const char *file_name, double font_size) {
    static const cairo_user_data_key_t key;

    if (file_name != NULL) {
        loaded_font_t *font;
        cairo_font_face_t *font_face;
        FT_Face face;
        nk_size index;

        index = font_atlas_find_font(file_name);
        if (~index) {
            font = font_atlas_font_get(index);
            font->usage++;
        }
        else {
            FT_Error ft_err;
            if (!ft_library) {
                FT_Init_FreeType(&ft_library);
            }
            ft_err = FT_New_Face(ft_library, file_name, 0, &face);
            if (ft_err) {
                fprintf(stderr, "%s[%d]: Font loading error: %s\n", __FILE__, __LINE__, FT_Error_String(ft_err));
                return nk_false;
            }
            index = nkfc_buffer_alloc(&font_atlas.fonts, (void **)&font);
            font->file = file_name;
            font->usage = 1;
            font->cairo = cairo_ft_font_face_create_for_ft_face(face, 0);
            cairo_font_face_set_user_data(font->cairo, &key, face, (cairo_destroy_func_t)FT_Done_Face);
        }
        font_atlas_alloc_face(nkfont, font, font_size);
        return nk_true;
    }
    return nk_false;
}

NK_API void nk_ft_cairo_font_free(struct nk_user_font *nkfont) {
    face_t *face;
    loaded_font_t *font;

    NKFC_ASSERT(nkfont);
    face = font_atlas_face_get(nkfont->userdata.id);
    if (face->scaled_font != NULL) {
        cairo_scaled_font_destroy(face->scaled_font);
    }
    font = face->font;
    if (font != NULL && --font->usage == 0) {
        cairo_font_face_destroy(font->cairo);
        memset(font, 0, sizeof(loaded_font_t));
    }
    memset(face, 0, sizeof(face_t));
}

NK_API cairo_scaled_font_t *nk_ft_cairo_font_set(cairo_t *cr, const struct nk_user_font *nkfont) {
    face_t *face;
    NKFC_ASSERT(cr);
    NKFC_ASSERT(nkfont);
    NKFC_ASSERT(nkfont->userdata.id < nkfc_buffer_size(&font_atlas.faces));
    NKFC_ASSERT(nkfont == font_atlas_face_get(nkfont->userdata.id)->nkfont);
    face = font_atlas_face_get(nkfont->userdata.id);
    NKFC_ASSERT(face->scaled_font);
    cairo_set_scaled_font(cr, face->scaled_font);
    return face->scaled_font;
}


NK_API void nk_ft_cairo_context_enter(cairo_t *cr, struct nk_context *ctx) {
    /* fixup fonts height and allocate scaled fonts */
    if (font_atlas.fixup) {
        cairo_font_extents_t extents;
        face_t *face;
        nk_size count = nkfc_buffer_size(&font_atlas.faces);
        for (nk_size i = 0; i < count; ++i) {
            face = font_atlas_face_get(i);
            if (face->font && face->font->cairo) {
                cairo_set_font_face(cr, face->font->cairo);
            }
            cairo_set_font_size(cr, face->font_size);
            face->scaled_font = cairo_get_scaled_font(cr);
            cairo_scaled_font_reference(face->scaled_font);
            cairo_scaled_font_extents(face->scaled_font, &extents);
            face->nkfont->height = extents.height;
        }
        font_atlas.fixup = nk_false;
    }
}

NK_API void nk_ft_cairo_context_leave(cairo_t *cr, struct nk_context *ctx) {
}

NK_API void nk_ft_cairo_render(cairo_t *cr, struct nk_context *ctx) {
    const struct nk_command *cmd = NULL;
    void *cmds = nk_buffer_memory(&ctx->memory);

    nk_foreach(cmd, ctx) {
        switch (cmd->type) {
        case NK_COMMAND_NOP:
            break;
        case NK_COMMAND_SCISSOR:
            {
                const struct nk_command_scissor *s = (const struct nk_command_scissor *)cmd;
                cairo_reset_clip(cr);
                if (s->x >= 0) {
                    cairo_rectangle(cr, s->x - 1, s->y - 1, s->w + 2, s->h + 2);
                    cairo_clip(cr);
                }
            }
            break;
        case NK_COMMAND_LINE:
            {
                const struct nk_command_line *l = (const struct nk_command_line *)cmd;
                cairo_set_source_rgba(cr,NK_COL2CAIRO(l->color.r),NK_COL2CAIRO(l->color.g),NK_COL2CAIRO(l->color.b),NK_COL2CAIRO(l->color.a));
                cairo_set_line_width(cr, l->line_thickness);
                cairo_move_to(cr, l->begin.x, l->begin.y);
                cairo_line_to(cr, l->end.x, l->end.y);
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_CURVE:
            {
                const struct nk_command_curve *q = (const struct nk_command_curve *)cmd;
                cairo_set_source_rgba(cr,NK_COL2CAIRO(q->color.r),NK_COL2CAIRO(q->color.g),NK_COL2CAIRO(q->color.b),NK_COL2CAIRO(q->color.a));
                cairo_set_line_width(cr, q->line_thickness);
                cairo_move_to(cr, q->begin.x, q->begin.y);
                cairo_curve_to(cr, q->ctrl[0].x, q->ctrl[0].y, q->ctrl[1].x, q->ctrl[1].y, q->end.x, q->end.y);
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_RECT:
            {
                const struct nk_command_rect *r = (const struct nk_command_rect *)cmd;
                cairo_set_source_rgba(cr,NK_COL2CAIRO(r->color.r),NK_COL2CAIRO(r->color.g),NK_COL2CAIRO(r->color.b),NK_COL2CAIRO(r->color.a));
                cairo_set_line_width(cr, r->line_thickness);
                if (r->rounding == 0) {
                    cairo_rectangle(cr, r->x, r->y, r->w, r->h);
                }
                else {
                    int xl = r->x + r->w - r->rounding;
                    int xr = r->x + r->rounding;
                    int yl = r->y + r->h - r->rounding;
                    int yr = r->y + r->rounding;
                    cairo_new_sub_path(cr);
                    cairo_arc(cr, xl, yr, r->rounding,NK_DEG2RAD(-90),NK_DEG2RAD(0));
                    cairo_arc(cr, xl, yl, r->rounding,NK_DEG2RAD(0),NK_DEG2RAD(90));
                    cairo_arc(cr, xr, yl, r->rounding,NK_DEG2RAD(90),NK_DEG2RAD(180));
                    cairo_arc(cr, xr, yr, r->rounding,NK_DEG2RAD(180),NK_DEG2RAD(270));
                    cairo_close_path(cr);
                }
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_RECT_FILLED:
            {
                const struct nk_command_rect_filled *r = (const struct nk_command_rect_filled *)cmd;
                cairo_set_source_rgba(cr,NK_COL2CAIRO(r->color.r),NK_COL2CAIRO(r->color.g),NK_COL2CAIRO(r->color.b),NK_COL2CAIRO(r->color.a));
                if (r->rounding == 0) {
                    cairo_rectangle(cr, r->x, r->y, r->w, r->h);
                } else {
                    int xl = r->x + r->w - r->rounding;
                    int xr = r->x + r->rounding;
                    int yl = r->y + r->h - r->rounding;
                    int yr = r->y + r->rounding;
                    cairo_new_sub_path(cr);
                    cairo_arc(cr, xl, yr, r->rounding,NK_DEG2RAD(-90),NK_DEG2RAD(0));
                    cairo_arc(cr, xl, yl, r->rounding,NK_DEG2RAD(0),NK_DEG2RAD(90));
                    cairo_arc(cr, xr, yl, r->rounding,NK_DEG2RAD(90),NK_DEG2RAD(180));
                    cairo_arc(cr, xr, yr, r->rounding,NK_DEG2RAD(180),NK_DEG2RAD(270));
                    cairo_close_path(cr);
                }
                cairo_fill(cr);
            }
            break;
        case NK_COMMAND_RECT_MULTI_COLOR:
            {
                /* from https://github.com/taiwins/twidgets/blob/master/src/nk_wl_cairo.c */
                const struct nk_command_rect_multi_color *r = (const struct nk_command_rect_multi_color *)cmd;
                cairo_pattern_t *pat = cairo_pattern_create_mesh();
                if (pat) {
                    cairo_mesh_pattern_begin_patch(pat);
                    cairo_mesh_pattern_move_to(pat, r->x, r->y);
                    cairo_mesh_pattern_line_to(pat, r->x, r->y + r->h);
                    cairo_mesh_pattern_line_to(pat, r->x + r->w, r->y + r->h);
                    cairo_mesh_pattern_line_to(pat, r->x + r->w, r->y);
                    cairo_mesh_pattern_set_corner_color_rgba(pat, 0,NK_COL2CAIRO(r->left.r),NK_COL2CAIRO(r->left.g),NK_COL2CAIRO(r->left.b),NK_COL2CAIRO(r->left.a));
                    cairo_mesh_pattern_set_corner_color_rgba(pat, 1,NK_COL2CAIRO(r->bottom.r),NK_COL2CAIRO(r->bottom.g),NK_COL2CAIRO(r->bottom.b),NK_COL2CAIRO(r->bottom.a));
                    cairo_mesh_pattern_set_corner_color_rgba(pat, 2,NK_COL2CAIRO(r->right.r),NK_COL2CAIRO(r->right.g),NK_COL2CAIRO(r->right.b),NK_COL2CAIRO(r->right.a));
                    cairo_mesh_pattern_set_corner_color_rgba(pat, 3,NK_COL2CAIRO(r->top.r),NK_COL2CAIRO(r->top.g),NK_COL2CAIRO(r->top.b),NK_COL2CAIRO(r->top.a));
                    cairo_mesh_pattern_end_patch(pat);

                    cairo_rectangle(cr, r->x, r->y, r->w, r->h);
                    cairo_set_source(cr, pat);
                    cairo_fill(cr);
                    cairo_pattern_destroy(pat);
                }
            }
            break;
        case NK_COMMAND_CIRCLE:
            {
                const struct nk_command_circle *c = (const struct nk_command_circle *)cmd;
                cairo_set_source_rgba(cr,NK_COL2CAIRO(c->color.r),NK_COL2CAIRO(c->color.g),NK_COL2CAIRO(c->color.b),NK_COL2CAIRO(c->color.a));
                cairo_set_line_width(cr, c->line_thickness);
                cairo_save(cr);
                cairo_translate(cr, c->x + c->w / 2.0, c->y + c->h / 2.0);
                cairo_scale(cr, c->w / 2.0, c->h / 2.0);
                cairo_arc(cr, 0, 0, 1,NK_DEG2RAD(0),NK_DEG2RAD(360));
                cairo_restore(cr);
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_CIRCLE_FILLED:
            {
                const struct nk_command_circle_filled *c = (const struct nk_command_circle_filled *)cmd;
                cairo_set_source_rgba(cr,NK_COL2CAIRO(c->color.r),NK_COL2CAIRO(c->color.g),NK_COL2CAIRO(c->color.b),NK_COL2CAIRO(c->color.a));
                cairo_save(cr);
                cairo_translate(cr, c->x + c->w / 2.0, c->y + c->h / 2.0);
                cairo_scale(cr, c->w / 2.0, c->h / 2.0);
                cairo_arc(cr, 0, 0, 1,NK_DEG2RAD(0),NK_DEG2RAD(360));
                cairo_restore(cr);
                cairo_fill(cr);
            }
            break;
        case NK_COMMAND_ARC:
            {
                const struct nk_command_arc *a = (const struct nk_command_arc*) cmd;
                cairo_set_source_rgba(cr,NK_COL2CAIRO(a->color.r),NK_COL2CAIRO(a->color.g),NK_COL2CAIRO(a->color.b),NK_COL2CAIRO(a->color.a));
                cairo_set_line_width(cr, a->line_thickness);
                cairo_arc(cr, a->cx, a->cy, a->r,NK_DEG2RAD(a->a[0]),NK_DEG2RAD(a->a[1]));
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_ARC_FILLED:
            {
                const struct nk_command_arc_filled *a = (const struct nk_command_arc_filled*)cmd;
                cairo_set_source_rgba(cr,NK_COL2CAIRO(a->color.r),NK_COL2CAIRO(a->color.g),NK_COL2CAIRO(a->color.b),NK_COL2CAIRO(a->color.a));
                cairo_arc(cr, a->cx, a->cy, a->r,NK_DEG2RAD(a->a[0]),NK_DEG2RAD(a->a[1]));
                cairo_fill(cr);
            }
            break;
        case NK_COMMAND_TRIANGLE:
            {
                const struct nk_command_triangle *t = (const struct nk_command_triangle *)cmd;
                cairo_set_source_rgba(cr,NK_COL2CAIRO(t->color.r),NK_COL2CAIRO(t->color.g),NK_COL2CAIRO(t->color.b),NK_COL2CAIRO(t->color.a));
                cairo_set_line_width(cr, t->line_thickness);
                cairo_move_to(cr, t->a.x, t->a.y);
                cairo_line_to(cr, t->b.x, t->b.y);
                cairo_line_to(cr, t->c.x, t->c.y);
                cairo_close_path(cr);
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_TRIANGLE_FILLED:
            {
                const struct nk_command_triangle_filled *t = (const struct nk_command_triangle_filled *)cmd;
                cairo_set_source_rgba(cr,NK_COL2CAIRO(t->color.r),NK_COL2CAIRO(t->color.g),NK_COL2CAIRO(t->color.b),NK_COL2CAIRO(t->color.a));
                cairo_move_to(cr, t->a.x, t->a.y);
                cairo_line_to(cr, t->b.x, t->b.y);
                cairo_line_to(cr, t->c.x, t->c.y);
                cairo_close_path(cr);
                cairo_fill(cr);
            }
            break;
        case NK_COMMAND_POLYGON:
            {
                int i;
                const struct nk_command_polygon *p = (const struct nk_command_polygon *)cmd;
                cairo_set_source_rgba(cr,NK_COL2CAIRO(p->color.r),NK_COL2CAIRO(p->color.g),NK_COL2CAIRO(p->color.b),NK_COL2CAIRO(p->color.a));
                cairo_set_line_width(cr, p->line_thickness);
                cairo_move_to(cr, p->points[0].x, p->points[0].y);
                for (i = 1; i < p->point_count; ++i) {
                    cairo_line_to(cr, p->points[i].x, p->points[i].y);
                }
                cairo_close_path(cr);
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_POLYGON_FILLED:
            {
                int i;
                const struct nk_command_polygon_filled *p = (const struct nk_command_polygon_filled *)cmd;
                cairo_set_source_rgba (cr,NK_COL2CAIRO(p->color.r),NK_COL2CAIRO(p->color.g),NK_COL2CAIRO(p->color.b),NK_COL2CAIRO(p->color.a));
                cairo_move_to(cr, p->points[0].x, p->points[0].y);
                for (i = 1; i < p->point_count; ++i) {
                    cairo_line_to(cr, p->points[i].x, p->points[i].y);
                }
                cairo_close_path(cr);
                cairo_fill(cr);
            }
            break;
        case NK_COMMAND_POLYLINE:
            {
                int i;
                const struct nk_command_polyline *p = (const struct nk_command_polyline *)cmd;
                cairo_set_source_rgba(cr,NK_COL2CAIRO(p->color.r),NK_COL2CAIRO(p->color.g),NK_COL2CAIRO(p->color.b),NK_COL2CAIRO(p->color.a));
                cairo_set_line_width(cr, p->line_thickness);
                cairo_move_to(cr, p->points[0].x, p->points[0].y);
                for (i = 1; i < p->point_count; ++i) {
                    cairo_line_to(cr, p->points[i].x, p->points[i].y);
                }
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_TEXT:
            {
                const struct nk_command_text *t = (const struct nk_command_text *)cmd;
                cairo_glyph_t *glyphs = NULL;
                int num_glyphs;
                cairo_text_cluster_t *clusters = NULL;
                int num_clusters;
                cairo_text_cluster_flags_t cluster_flags;
                cairo_font_extents_t extents;
                cairo_scaled_font_t *scaled_font;

                cairo_set_source_rgba(cr,NK_COL2CAIRO(t->foreground.r),NK_COL2CAIRO(t->foreground.g),NK_COL2CAIRO(t->foreground.b),NK_COL2CAIRO(t->foreground.a));
                scaled_font = nk_ft_cairo_font_set(cr, t->font);
                cairo_scaled_font_extents(scaled_font, &extents);
                cairo_scaled_font_text_to_glyphs(scaled_font, t->x, t->y + extents.ascent, t->string, t->length, &glyphs, &num_glyphs, &clusters, &num_clusters, &cluster_flags);
                cairo_show_text_glyphs(cr, t->string, t->length, glyphs, num_glyphs, clusters, num_clusters, cluster_flags);
                cairo_glyph_free(glyphs);
                cairo_text_cluster_free(clusters);
            }
            break;
        case NK_COMMAND_IMAGE:
            {
                /* from https://github.com/taiwins/twidgets/blob/master/src/nk_wl_cairo.c */
                const struct nk_command_image *im = (const struct nk_command_image *)cmd;
                cairo_surface_t *img_surf;
                double sw = (double)im->w / (double)im->img.region[2];
                double sh = (double)im->h / (double)im->img.region[3];
                cairo_format_t format = CAIRO_FORMAT_ARGB32;
                int stride = cairo_format_stride_for_width(format, im->img.w);

                if (!im->img.handle.ptr) break;
                img_surf = cairo_image_surface_create_for_data(im->img.handle.ptr, format, im->img.w, im->img.h, stride);
                if (!img_surf) break;
                cairo_save(cr);

                cairo_rectangle(cr, im->x, im->y, im->w, im->h);
                /* scale here, if after source set, the scale would not apply to source
                 * surface
                 */
                cairo_scale(cr, sw, sh);
                /* the coordinates system in cairo is not intuitive, scale, translate,
                 * are applied to source. Refer to
                 * "https://www.cairographics.org/FAQ/#paint_from_a_surface" for details
                 *
                 * if you set source_origin to (0,0), it would be like source origin
                 * aligned to dest origin, then if you draw a rectangle on (x, y, w, h).
                 * it would clip out the (x, y, w, h) of the source on you dest as well.
                 */
                cairo_set_source_surface(cr, img_surf, im->x/sw - im->img.region[0], im->y/sh - im->img.region[1]);
                cairo_fill(cr);

                cairo_restore(cr);
                cairo_surface_destroy(img_surf);
            }
            break;
        case NK_COMMAND_CUSTOM:
            {
	            const struct nk_command_custom *cu = (const struct nk_command_custom *)cmd;
                if (cu->callback) {
                    cu->callback(cr, cu->x, cu->y, cu->w, cu->h, cu->callback_data);
                }
            }
            break;
        default:
            break;
        }
    }
}

#ifdef __cplusplus
}
#endif

#endif /* NKFC_IMPLEMENTATION */
