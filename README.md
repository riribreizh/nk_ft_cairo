A Freetype/Cairo render backend for Nuklear.

The Cairo implementation for Nuklear (drawing commands) is heavily inspired by Adriano Grieb's [nuklear_xcb](https://github.com/griebd/nuklear_xcb).

It is independant of any windowing system so the same rendering backend can be used with any of them (glfw, pugl, xlib, etc...).

## Put it in your project

If you want to provide your own windowing code, then the header file is all what you need. Like other header only libraries, you place a define before including it in one C/C++ file in your project to compile the definitions of the functions:

```c
#define NKFC_IMPLEMENTATION
#include <nk_ft_cairo.h>
```

Then you can add the header everywhere else
```c
#include <nk_ft_cairo.h>

/* use functions */
```

### CMake

If you use CMake, you can use the `nk_ft_cairo` interface library provided target. It will add the path to the header to the target.

```cmake
add_subdirectory(nk_ft_cairo)

target_link_libraries(my_executable nk_ft_cairo)
```

Note that in every case, Cairo's main header file will be included (needed for the `cairo_t` type) from its standard location `<cairo/cairo.h>` if not already included (tests the define `CAIRO_H`).

If Nuklear is not included before, it will also be automatically included without any `NK_*` define. So always prefer to include `<nuklear.h>` yourself before.

In the implementation, `<cairo/cairo-ft.h>` will be included the same way if not already included (tests the define `CAIRO_FT_H`).

## Using

This library has two goals:
* provide an implementation to render Nuklear draw commands with Cairo
* provide a simple to use and lightweight font atlas using Freetype

Before anything, the library has to be initialized (and terminated at the end):
```c
int main(int argc, char *argv[]) {
    /* the parameter is to preallocate space for fonts to load.
     * if you plan to use only one font, just set 1. If you know
     * in advance how many font files you will load, set this
     * number here. The font atlas grows automatically with a grow
     * factor of 2 when needed (can be customized, see below).
     */
    nk_ft_cairo_init(1);

    /* code */

    nk_ft_cairo_term();
}
```

Then you have to define at least one font (specifying its size). If you don't want to use an external file, you can use the default Cairo font by just specifying its size.

In either case, you provide a pointer to a `struct nk_user_font` to receive the font handle (and initialize the Nuklear context with one of them).
```c
    struct nk_context ctx;
    struct nk_user_font user_font;
    double font_size = 12.0;

    nk_ft_cairo_font_load(&user_font, font_size);
    nk_ft_cairo_font_from_file(&ttf_font, "path/to/file.ttf", 16.0);
    nk_init_default(&ctx, &user_font);
```

Note that because it is using Freetype to handle fonts, it's not possible to use the Nuklear integrated font for now. Future work might permit to bypass this.

> If you use the same font file with different sizes, it will be loaded only once, and the correct Cairo scaled font will be allocated from the same one face.

Finally, when it's about to draw things, you prepare the graphic context to get a Cairo context `cairo_t` and enter the context, draw your Nuklear UI, use the render function and leave the context.

```c
    struct cairo_t cr;
    /* the cairo context must be valid here
     * it's up to you to get it from a surface
     */
    nk_ft_cairo_context_enter(cr, &ctx);
    /* draw UI */
    if (nk_begin(&ctx, ...)) {
    }
    nk_end(&ctx);
    /* render */
    nk_ft_cairo_render(cr, &ctx);
    /* finish */
    nk_clear(&ctx);
    nk_ft_cairo_context_leave(cr, &ctx);
```

As a facility, two macros are available to be reused for your own Cairo calls (see the demo to see how it's used to set the background color of the window):
* `NK_DEG2RAD(x)`: returns a `double` degrees value converted to radians
* `NK_COL2CAIRO(x)`: returns a normalized `double` from a RGBA integer (range `0` to `255`)


## Customize

### NKFC_DEFAULT_FONT_SIZE

If you pass `0` to a font loading function, a default font size will be applied with the value of this define (default value is `12.0`). You can override this by defining it before including the library.

### NKFC_BUFFER_GROW_FACTOR

The font atlas grows by a factor of 2 when it needs to reallocate memory. You can change this by defining this before including the library.

### NKFC_ASSERT

Uses `NK_ASSERT` by default, or `assert` if it does not exist.

### Nuklear macros

The library reuses some of the Nuklear defines, so if you override them they will be also applied:
* `NK_API` for API functions
* `NK_INTERN` for internal (implementation) functions

## API

### nk_ft_cairo_init(nk_size initial_fonts_capacity)

Call this before using any library function. This initializes the font atlas, optionally preallocating memory for a given number of
fonts.

### nk_ft_cairo_term()

Frees memory and releases Cairo/Freetype font handles.

### nk_bool nk_ft_cairo_font_load(struct nk_user_font *font, double font_size)

Loads the Cairo default font into the font atlas with the given size. If size is `0`, `NKFC_DEFAULT_FONT_SIZE` value will be used.

The Nukear user font is filled with a reference to this font (memory for the structure must exist).

### nk_bool nk_ft_cairo_font_from_file(struct nk_user_font *font, const char *file_name, double font_size)

Loads the TTF file (or any font file type supported by Freetype) into the font atlas with the given size. If size is `0`, `NKFC_DEFAULT_FONT_SIZE` value will be used.

If the same font file is requested several times for different font sizes, only one face (reference to the font file content) will be reused for the scaled font handles.

The Nukear user font is filled with a reference to this font (memory for the structure must exist).

### void nk_ft_cairo_font_free(struct nk_user_font *font)

Usually not needed, unless you have complex font handling. If you know you won't use a font anymore, you can call this using a previously initialized Nuklear user font.

### cairo_scaled_font_t *nk_ft_cairo_font_set(cairo_t *cr, const struct nk_user_font *font)

Changes the new default font. This is used internally by the rendering function when Nuklear calls change the current Nuklear user font. But you can use it too if needed.

### void nk_ft_cairo_context_enter(cairo_t *cr, struct nk_context *ctx)

To be called in the application loop (or event handler) before drawing anything (even before using Nuklear UI function).

The Cairo context must be valid (initialized from a Cairo surface) before calling this, as this context will be used to compute the fonts height for this specific surface.

> The Nuklear context is not used for now, so you could pass `NULL`, but in case it's needed in the future, it's better to specify your real context.

### void nk_ft_cairo_context_leave(cairo_t *cr, struct nk_context *ctx)

Once the render is done, call this function to *leave* the rendering context.

> Actually this function is empty for now, but call it in your code in case someday its presence becomes mandatory.

### void nk_ft_cairo_render(cairo_t *cr, struct nk_context *ctx)

That's what will translate Nuklear drawing commands into Cairo canvas drawing functions.

## Demo

## License

nk_ft_cairo is copyleft 2021 Richard Gill, under the GNU GPL 3.0 or above license. See [Copying](./COPYING).

It makes use of valuable parts of different projects:
* [nuklear_xcb](https://github.com/griebd/nuklear_xcb) for cairo integration (MIT License)
  Copyright 2017 Adriano Grieb
* [twidgets](https://github.com/taiwins/twidgets) for cairo implementation of NK_COMMAND_RECT_MULTI_COLOR (MIT License)
  Copyright (c) 2019-2020 Xichen Zhou

Made for use with:
* [Nuklear](https://github.com/Immediate-Mode-UI/Nuklear)
  Under Public Domain or MIT License
* [Pugl](https://gitlab.com/lv2/pugl)
  Under ISC License
* [Freetype](https://www.freetype.org/)
  Under Freetype License (FTL) or GNU General Public License (GPL) version 2
* [Cairo](https://www.cairographics.org/)
  Under GNU Lesser General Public License (LGPL) version 2.1 or the Mozilla Public License (MPL) version 1.1
