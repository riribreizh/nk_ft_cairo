
# configure with Cairo backend only

find_package(Cairo REQUIRED)

set(pugl_SOURCE_DIR ${THIRDPARTY_DIR}/pugl)
set(pugl_HEADERS_DIR "${pugl_SOURCE_DIR}/include")

add_library(pugl STATIC
    ${pugl_HEADERS_DIR}/pugl/pugl.h
    ${pugl_HEADERS_DIR}/pugl/stub.h
    ${pugl_HEADERS_DIR}/pugl/cairo.h
    ${pugl_SOURCE_DIR}/src/types.h
    ${pugl_SOURCE_DIR}/src/stub.h
    ${pugl_SOURCE_DIR}/src/implementation.h
    ${pugl_SOURCE_DIR}/src/implementation.c
)
set_target_properties(pugl PROPERTIES
    C_STANDARD 99
    C_STANDARD_REQUIRED TRUE
    C_EXTENSIONS FALSE
)
target_include_directories(pugl PUBLIC ${pugl_HEADERS_DIR} ${CAIRO_INCLUDE_DIR})
target_link_libraries(pugl ${CAIRO_LIBRARIES})
target_compile_definitions(pugl PUBLIC PUGL_DISABLE_DEPRECATED PUGL_STATIC)
target_compile_definitions(pugl PRIVATE PUGL_INTERNAL)

if(WIN32)
    target_sources(pugl PRIVATE
        ${pugl_SOURCE_DIR}/src/win.h
        ${pugl_SOURCE_DIR}/src/win.c
        ${pugl_SOURCE_DIR}/src/win_stub.c
        ${pugl_SOURCE_DIR}/src/win_cairo.c
    )
    # TODO deps
elseif(LINUX)
    find_package(X11 REQUIRED)
    target_sources(pugl PRIVATE
        ${pugl_SOURCE_DIR}/src/x11.h
        ${pugl_SOURCE_DIR}/src/x11.c
        ${pugl_SOURCE_DIR}/src/x11_stub.c
        ${pugl_SOURCE_DIR}/src/x11_cairo.c
    )
    target_link_libraries(pugl m X11::X11)
    # check for X extensions
    if(X11_Xcursor_FOUND)
        target_compile_definitions(pugl PRIVATE HAVE_XCURSOR)
        target_link_libraries(pugl X11::Xcursor)
    endif()
    if(X11_Xrandr_FOUND)
        target_compile_definitions(pugl PRIVATE HAVE_XRANDR)
        target_link_libraries(pugl X11::Xrandr)
    endif()
    if (X11_Xext_LIB AND X11_XSync_FOUND)
        target_compile_definitions(pugl PRIVATE HAVE_XSYNC)
        target_link_libraries(pugl X11::Xext)
    endif()
elseif(APPLE)
    target_sources(pugl PRIVATE
        ${pugl_SOURCE_DIR}/src/mac.h
        ${pugl_SOURCE_DIR}/src/mac.m
        ${pugl_SOURCE_DIR}/src/mac_stub.m
        ${pugl_SOURCE_DIR}/src/mac_cairo.m
    )
    # TODO deps
endif()
