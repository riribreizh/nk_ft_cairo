
set(nuklear_SOURCE_DIR ${THIRDPARTY_DIR}/nuklear)

add_library(nuklear INTERFACE ${nuklear_SOURCE_DIR}/nuklear.h)
target_include_directories(nuklear INTERFACE ${nuklear_SOURCE_DIR})
target_compile_definitions(nuklear INTERFACE
    NK_INCLUDE_FIXED_TYPES
    NK_INCLUDE_DEFAULT_ALLOCATOR
    NK_INCLUDE_STANDARD_IO
    NK_INCLUDE_STANDARD_VARARGS
    NK_INCLUDE_STANDARD_BOOL
)
