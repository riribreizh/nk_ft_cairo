#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <cairo/cairo.h>
#define NK_IMPLEMENTATION
#include <nuklear.h>
#define NKFC_IMPLEMENTATION
#include <nk_ft_cairo.h>
#include "glfw_cairo.h"

#ifdef _WIN32
#include <windows.h>
#endif
#include <time.h>

typedef struct app {
    GLFWwindow *win;
    struct nk_context ctx;
    cairo_surface_t *surf;

    struct nk_color bg;
    struct nk_user_font default_font;
} app_t;

static void app_draw(app_t *app, int width, int height) {
    struct nk_context *ctx = &app->ctx;
    struct nk_rect rc = {width-250, 0, 250, height};

    if (nk_begin(ctx, "test", nk_rect(0, 0, 200, 200), NK_WINDOW_TITLE|NK_WINDOW_MOVABLE|NK_WINDOW_MINIMIZABLE)) {
        nk_layout_row_dynamic(ctx, 0, 1);
        nk_label(ctx, "Label", NK_TEXT_ALIGN_LEFT);
    }
    nk_end(ctx);

    if (nk_begin(ctx, "Demo", rc, 0)) {
        enum {EASY, HARD};
        static int op = EASY;
        static int property = 20;

        nk_layout_row_static(ctx, 30, 80, 1);
        if (nk_button_label(ctx, "button"))
            fprintf(stdout, "button pressed\n");
        nk_layout_row_dynamic(ctx, 30, 2);
        if (nk_option_label(ctx, "easy", op == EASY)) op = EASY;
        if (nk_option_label(ctx, "hard", op == HARD)) op = HARD;
        nk_layout_row_dynamic(ctx, 25, 1);
        nk_property_int(ctx, "Compression:", 0, &property, 100, 10, 1);

        nk_layout_row_dynamic(ctx, 20, 1);
        nk_label(ctx, "background:", NK_TEXT_LEFT);
        nk_layout_row_dynamic(ctx, 25, 1);
        if (nk_combo_begin_color(ctx, app->bg, nk_vec2(nk_widget_width(ctx),400))) {
            nk_layout_row_dynamic(ctx, 120, 1);
            app->bg = nk_rgb_cf(nk_color_picker(ctx, nk_color_cf(app->bg), NK_RGBA));
            nk_layout_row_dynamic(ctx, 25, 1);
            app->bg.r = nk_propertyf(ctx, "#R:", 0, app->bg.r, 1.0f, 0.01f,0.005f);
            app->bg.g = nk_propertyf(ctx, "#G:", 0, app->bg.g, 1.0f, 0.01f,0.005f);
            app->bg.b = nk_propertyf(ctx, "#B:", 0, app->bg.b, 1.0f, 0.01f,0.005f);
            app->bg.a = nk_propertyf(ctx, "#A:", 0, app->bg.a, 1.0f, 0.01f,0.005f);
            nk_combo_end(ctx);
        }
    }
    nk_end(ctx);
}

static const double rate = (1 / 60.0f);

static void sleeper(double s) {
    if (s > 0.0f) {
        uint64_t ms = (uint64_t)(s * 1000);
        printf("sleep %lu ms\n", ms); fflush(stdout);
#ifdef _WIN32
        Sleep(ms);
#else
        struct timespec ts;
        ts.tv_sec = ms / 1000;
        ts.tv_nsec = (ms % 1000) * 1000000;
        nanosleep(&ts, NULL);
#endif
    }
}

static void error_callback(int code, const char *desc) {
    fprintf(stderr, "GLFW error[%d]: %s\n", code, desc);
}

static void app_render(app_t *app) {
    int width, height;
    cairo_t *cr;
    cairo_surface_t *front;
    cairo_surface_t *back;
    glfwGetWindowSize(app->win, &width, &height);

    // init context
    back = glfw_cairo_create_surface(app->win, width, height);
    front = cairo_surface_create_similar(back, cairo_surface_get_content(back), width, height);
    cr = cairo_create(front);

    // clear with background color
    cairo_rectangle(cr, 0, 0, width, height);
    cairo_set_source_rgb(cr, NK_COL2CAIRO(app->bg.r), NK_COL2CAIRO(app->bg.g), NK_COL2CAIRO(app->bg.b));
    cairo_fill(cr);

    // draw app
    nk_ft_cairo_context_enter(cr, &app->ctx);
    app_draw(app, width, height);
    nk_ft_cairo_render(cr, &app->ctx);
    nk_ft_cairo_context_leave(cr, &app->ctx);
    nk_clear(&app->ctx);

    // cleanup context
    cairo_destroy(cr);
    cr = cairo_create(back);
    cairo_rectangle(cr, 0, 0, width, height);
    cairo_clip(cr);
    cairo_set_source_surface(cr, front, 0, 0);
    cairo_paint(cr);
    cairo_destroy(cr);
    cairo_surface_flush(back);
    cairo_surface_destroy(front);
    cairo_surface_destroy(back);
}

static void resize_cb(GLFWwindow *win, int width, int height) {
    app_t *app = glfwGetWindowUserPointer(win);
    glfw_cairo_resize_surface(app->surf, width, height);
}

static void mouse_button_cb(GLFWwindow *win, int button, int action, int mods) {
    struct nk_context *ctx = &((app_t *)glfwGetWindowUserPointer(win))->ctx;
    double x, y;
    glfwGetCursorPos(win, &x, &y);
    nk_input_button(ctx, button, (int)x, (int)y, action == GLFW_PRESS);
}

static void mouse_motion_cb(GLFWwindow *win, double x, double y) {
    struct nk_context *ctx = &((app_t *)glfwGetWindowUserPointer(win))->ctx;
    nk_input_motion(ctx, (int)x, (int)y);
}

static void mouse_scroll_cb(GLFWwindow *win, double dx, double dy) {
    struct nk_context *ctx = &((app_t *)glfwGetWindowUserPointer(win))->ctx;
    nk_input_scroll(ctx, nk_vec2(dx, dy));
}

static void char_cb(GLFWwindow *win, unsigned int codepoint) {
    struct nk_context *ctx = &((app_t *)glfwGetWindowUserPointer(win))->ctx;
    nk_input_unicode(ctx, codepoint);
}

static void translate_glfw_keys(GLFWwindow *win, struct nk_context *ctx) {
    nk_input_key(ctx, NK_KEY_DEL, glfwGetKey(win, GLFW_KEY_DELETE) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_ENTER, glfwGetKey(win, GLFW_KEY_ENTER) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_TAB, glfwGetKey(win, GLFW_KEY_TAB) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_BACKSPACE, glfwGetKey(win, GLFW_KEY_BACKSPACE) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_UP, glfwGetKey(win, GLFW_KEY_UP) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_DOWN, glfwGetKey(win, GLFW_KEY_DOWN) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_TEXT_START, glfwGetKey(win, GLFW_KEY_HOME) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_TEXT_END, glfwGetKey(win, GLFW_KEY_END) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_SCROLL_START, glfwGetKey(win, GLFW_KEY_HOME) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_SCROLL_END, glfwGetKey(win, GLFW_KEY_END) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_SCROLL_DOWN, glfwGetKey(win, GLFW_KEY_PAGE_DOWN) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_SCROLL_UP, glfwGetKey(win, GLFW_KEY_PAGE_UP) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_SHIFT, glfwGetKey(win, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS||
                                    glfwGetKey(win, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS);

    if (glfwGetKey(win, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS ||
        glfwGetKey(win, GLFW_KEY_RIGHT_CONTROL) == GLFW_PRESS) {
        nk_input_key(ctx, NK_KEY_COPY, glfwGetKey(win, GLFW_KEY_C) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_PASTE, glfwGetKey(win, GLFW_KEY_V) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_CUT, glfwGetKey(win, GLFW_KEY_X) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_TEXT_UNDO, glfwGetKey(win, GLFW_KEY_Z) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_TEXT_REDO, glfwGetKey(win, GLFW_KEY_R) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_TEXT_WORD_LEFT, glfwGetKey(win, GLFW_KEY_LEFT) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_TEXT_WORD_RIGHT, glfwGetKey(win, GLFW_KEY_RIGHT) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_TEXT_LINE_START, glfwGetKey(win, GLFW_KEY_B) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_TEXT_LINE_END, glfwGetKey(win, GLFW_KEY_E) == GLFW_PRESS);
    }
    else {
        nk_input_key(ctx, NK_KEY_LEFT, glfwGetKey(win, GLFW_KEY_LEFT) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_RIGHT, glfwGetKey(win, GLFW_KEY_RIGHT) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_COPY, 0);
        nk_input_key(ctx, NK_KEY_PASTE, 0);
        nk_input_key(ctx, NK_KEY_CUT, 0);
        nk_input_key(ctx, NK_KEY_SHIFT, 0);
    }
}

int main(int argc, char *argv[]) {
    app_t app;
    double frame_time, last_time;

    memset(&app, 0, sizeof(app_t));
    app.bg = nk_rgb(0, 30, 30);

    glfwSetErrorCallback(error_callback);
    glfwInit();

    nk_ft_cairo_init(1);
    nk_ft_cairo_font_from_file(&app.default_font, "../demo/DroidSans.ttf", 14.0);
    nk_init_default(&app.ctx, &app.default_font);

    /* create the window */
    glfwWindowHint(GLFW_RESIZABLE, true);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    app.win = glfwCreateWindow(800, 600, "Glfw/Freetype/Cairo/Nuklear demo", NULL, NULL);
    {
        int width, height;
        glfwGetWindowSize(app.win, &width, &height);
        app.surf = glfw_cairo_create_surface(app.win, width, height);
    }
    glfwSetWindowUserPointer(app.win, &app);
    glfwSetWindowSizeCallback(app.win, resize_cb);
    glfwSetMouseButtonCallback(app.win, mouse_button_cb);
    glfwSetCursorPosCallback(app.win, mouse_motion_cb);
    glfwSetScrollCallback(app.win, mouse_scroll_cb);
    glfwSetCharCallback(app.win, char_cb);

    glfwShowWindow(app.win);
    /* run the loop */
    while (!glfwWindowShouldClose(app.win)) {
        last_time = glfwGetTime();
        nk_input_begin(&app.ctx);
        glfwWaitEvents();
        translate_glfw_keys(app.win, &app.ctx);
        nk_input_end(&app.ctx);

        app_render(&app);

        frame_time = glfwGetTime() - last_time;
        sleeper(rate - frame_time);
    }

    nk_ft_cairo_term();
    cairo_surface_destroy(app.surf);
    glfwDestroyWindow(app.win);
    glfwTerminate();
    return 0;
}
