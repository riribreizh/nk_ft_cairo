#ifndef DEMO_GLFW_GLFW_CAIRO_H
#define DEMO_GLFW_GLFW_CAIRO_H

#if defined(_WIN32)
#define IS_WINDOWS
#define GLFW_EXPOSE_NATIVE_WIN32
#include <cairo/cairo-win32.h>
#elif defined(__APPLE__)
#define IS_COCOA
#define GLFW_EXPOSE_NATIVE_COCOA
#include <cairo/cairo-quartz.h>
#else
#define IS_X11
#define GLFW_EXPOSE_NATIVE_X11
#include <cairo/cairo-xlib.h>
// no support for Wayland (yet)
#endif
#include <cairo/cairo.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

#ifdef IS_X11
cairo_surface_t *glfw_cairo_create_surface(GLFWwindow *win, int width, int height) {
    Display *dpy = glfwGetX11Display();
    int screen;
    Window xwin;

    return cairo_xlib_surface_create(dpy, (Drawable)glfwGetX11Window(win), DefaultVisual(dpy, DefaultScreen(dpy)), width, height);
}

void glfw_cairo_resize_surface(cairo_surface_t *surf, int width, int height) {
    cairo_xlib_surface_set_size(surf, width, height);
}
#endif

#endif /* !DEMO_GLFW_GLFW_CAIRO_H */
