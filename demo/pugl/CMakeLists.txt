
find_package(Freetype REQUIRED)

include(build_pugl)

add_executable(pugl-demo nk_pugl.h main.c)
target_compile_definitions(pugl-demo PRIVATE
    # nanosleep
    _POSIX_C_SOURCE=199309L
)
target_link_libraries(pugl-demo pugl nuklear nk_ft_cairo Freetype::Freetype)
set_target_properties(pugl-demo PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})
