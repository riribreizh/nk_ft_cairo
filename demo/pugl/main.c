#include <stdbool.h>
#include <string.h>
#include <pugl/pugl.h>
#include <pugl/cairo.h>
#define NK_IMPLEMENTATION
#include <nuklear.h>
#define NKFC_IMPLEMENTATION
#include <nk_ft_cairo.h>

#ifdef _WIN32
#include <windows.h>
#endif
#include <time.h>

typedef struct app {
    PuglView *view;
    struct nk_context ctx;
    bool running;

    struct nk_color bg;
    struct nk_user_font default_font;
} app_t;

static void app_draw(app_t *app, int width, int height) {
    struct nk_context *ctx = &app->ctx;
    struct nk_rect rc = {width-250, 0, 250, height};

    if (nk_begin(ctx, "test", nk_rect(0, 0, 200, 200), NK_WINDOW_TITLE|NK_WINDOW_MOVABLE|NK_WINDOW_MINIMIZABLE)) {
        nk_layout_row_dynamic(ctx, 0, 1);
        nk_label(ctx, "Label", NK_TEXT_ALIGN_LEFT);
    }
    nk_end(ctx);

    if (nk_begin(ctx, "Demo", rc, 0)) {
        enum {EASY, HARD};
        static int op = EASY;
        static int property = 20;

        nk_layout_row_static(ctx, 30, 80, 1);
        if (nk_button_label(ctx, "button"))
            fprintf(stdout, "button pressed\n");
        nk_layout_row_dynamic(ctx, 30, 2);
        if (nk_option_label(ctx, "easy", op == EASY)) op = EASY;
        if (nk_option_label(ctx, "hard", op == HARD)) op = HARD;
        nk_layout_row_dynamic(ctx, 25, 1);
        nk_property_int(ctx, "Compression:", 0, &property, 100, 10, 1);

        nk_layout_row_dynamic(ctx, 20, 1);
        nk_label(ctx, "background:", NK_TEXT_LEFT);
        nk_layout_row_dynamic(ctx, 25, 1);
        if (nk_combo_begin_color(ctx, app->bg, nk_vec2(nk_widget_width(ctx),400))) {
            nk_layout_row_dynamic(ctx, 120, 1);
            app->bg = nk_rgb_cf(nk_color_picker(ctx, nk_color_cf(app->bg), NK_RGBA));
            nk_layout_row_dynamic(ctx, 25, 1);
            app->bg.r = nk_propertyf(ctx, "#R:", 0, app->bg.r, 1.0f, 0.01f,0.005f);
            app->bg.g = nk_propertyf(ctx, "#G:", 0, app->bg.g, 1.0f, 0.01f,0.005f);
            app->bg.b = nk_propertyf(ctx, "#B:", 0, app->bg.b, 1.0f, 0.01f,0.005f);
            app->bg.a = nk_propertyf(ctx, "#A:", 0, app->bg.a, 1.0f, 0.01f,0.005f);
            nk_combo_end(ctx);
        }
    }
    nk_end(ctx);
}

static PuglStatus handle_events(PuglView *view, const PuglEvent *event) {
    app_t *app = puglGetHandle(view);
    struct nk_context *ctx = &app->ctx;
    switch (event->type) {
    case PUGL_CREATE: {
        nk_input_begin(ctx);
    } break;
    case PUGL_DESTROY: {
        nk_input_end(ctx);
    } break;
    case PUGL_CLOSE: {
        app->running = false;
    } break;
    case PUGL_EXPOSE: {
        /* prepare */
        cairo_t *cr = puglGetContext(view);
        nk_ft_cairo_context_enter(cr, ctx);
        nk_input_end(ctx);
        /*nk_ft_cairo_font_set(cr, &app->default_font);*/
        /* set background color */
        cairo_rectangle(cr, 0, 0, event->expose.width, event->expose.height);
        cairo_set_source_rgb(cr, NK_COL2CAIRO(app->bg.r), NK_COL2CAIRO(app->bg.g), NK_COL2CAIRO(app->bg.b));
        cairo_fill(cr);
        /* draw UI */
        app_draw(app, (int)event->expose.width, (int)event->expose.height);
        /* render the whole */
        nk_ft_cairo_render(cr, ctx);
        /* finalize */
        nk_clear(ctx);
        nk_input_begin(ctx);
        nk_ft_cairo_context_leave(cr, ctx);
    } break;
    case PUGL_TEXT: {
        nk_input_unicode(ctx, event->text.character);
        puglPostRedisplay(view);
    } break;
    case PUGL_BUTTON_PRESS:
    case PUGL_BUTTON_RELEASE: {
        nk_input_button(ctx, event->button.button - 1, event->button.x, event->button.y, (event->type == PUGL_BUTTON_PRESS));
        puglPostRedisplay(view);
    } break;
    case PUGL_MOTION: {
        nk_input_motion(ctx, event->motion.x, event->motion.y);
        puglPostRedisplay(view);
    } break;
    case PUGL_SCROLL: {
        nk_input_scroll(ctx, nk_vec2(event->scroll.dx, event->scroll.dy));
        puglPostRedisplay(view);
    } break;
    default:
        break;
    }
    return PUGL_SUCCESS;
}

static const double rate = (1 / 60.0f);

static void sleeper(double s) {
    if (s > 0.0f) {
        uint64_t ms = (uint64_t)(s * 1000);
        printf("sleep %lu ms\n", ms); fflush(stdout);
#ifdef _WIN32
        Sleep(ms);
#else
        struct timespec ts;
        ts.tv_sec = ms / 1000;
        ts.tv_nsec = (ms % 1000) * 1000000;
        nanosleep(&ts, NULL);
#endif
    }
}

int main(int argc, char *argv[]) {
    app_t app;
    double frame_time, last_time;
    PuglWorld *world;

    memset(&app, 0, sizeof(app_t));
    app.running = true;
    app.bg = nk_rgb(0, 30, 30);

    /* create the view */
    world = puglNewWorld(PUGL_PROGRAM, 0);
    app.view = puglNewView(world);
    nk_ft_cairo_init(2);

    puglSetHandle(app.view, &app);
    puglSetWindowTitle(app.view, "Pugl/Freetype/Cairo/Nuklear demo");
    puglSetViewHint(app.view, PUGL_RESIZABLE, true);
    puglSetViewHint(app.view, PUGL_IGNORE_KEY_REPEAT, true);
    puglSetEventFunc(app.view, handle_events);
    puglSetFrame(app.view, (PuglRect){0, 0, 800, 600});
    puglSetBackend(app.view, puglCairoBackend());
    puglRealize(app.view);

    nk_ft_cairo_font_from_file(&app.default_font, "../demo/DroidSans.ttf", 14.0);
    nk_init_default(&app.ctx, &app.default_font);

    puglShow(app.view);
    while (app.running) {
        last_time = puglGetTime(world);
        puglUpdate(world, -1.0f);
        frame_time = puglGetTime(world) - last_time;
        sleeper(rate - frame_time);
    }

    nk_ft_cairo_term();
    puglFreeView(app.view);
    puglFreeWorld(world);
    return 0;
}
